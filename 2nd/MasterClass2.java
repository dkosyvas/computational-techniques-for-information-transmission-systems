package demo;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import jade.core.AID;
import jade.core.Agent;
import jade.core.AgentContainer;
import jade.core.Location;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;


public class MasterClass2  extends Agent{
					@Override
						protected void setup() {
							System.out.println("eimai o Master "+getLocalName()+"kai ime zwntanos");
								String MyName = getLocalName();
								addBehaviour(new OneShotBehaviour(this) {
											@Override
												public void action() {
												jade.core.Runtime runtime = jade.core.Runtime.instance();
													Profile profile = new ProfileImpl();
													profile.setParameter(Profile.CONTAINER_NAME, "PrwtoContainer");
													profile.setParameter(Profile.MAIN_HOST, "localhost");
													//dhmioyrgw container
													ContainerController container = runtime.createAgentContainer(profile);
													/*
													 * Dhmioyrgw 20 workers me onoma bot0 , bot1 ...etc bot19
													 * Toys dinw san eisodo thn metavallomenh aktina ths voh8htikhs epifaneias
													 *  0.01, 0.0616, 0.1132, 0.1647, 0.2163, 0.2679, 0.3195, 0.3711, 0.4226, 0.4742, 0.5258, 0.5774, 0.6289, 0.6805, 0.7321, 0.7837, 0.8353, 0.8868, 0.9384, 0.99]
													 */
													for (int i=0 ; i<20;i++) {
														try {
															AgentController agen = container.createNewAgent("bot"+String.valueOf(i),
																	"start.WorkerClass",new Object[] {0.01 + 0.0516*i});//arguments
															agen.start();
														} catch (StaleProxyException ex) {
																ex.printStackTrace();
														}
													}
											}
								});
					}
	}