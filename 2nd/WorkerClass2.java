package demo;
import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jade.core.Agent;
import jade.core.Location;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;
public class WorkerClass2 extends Agent{
	@Override
		protected void setup() {
			Object[] args = getArguments();

			String number = String.valueOf(args[0]);
				addBehaviour(new OneShotBehaviour(this) {
						@Override
						public void action() {

							/*
							 * briskw to ektelesimo arxeio kai to trexw
							 */
							ProcessBuilder pb = new ProcessBuilder("java","-jar","/home/jim/Music/2h/ex2.jar",number);
							pb.redirectErrorStream();
							/*
							 * diavazw thn eksodo toy ektelesimoy poy einai 
							 * h aktina ths voh8htikhs epifaneias kai to sfalma oriakwn syn8hkwn Debc
							 */
							ArrayList<String> eksodos = new ArrayList<String>();
								InputStream cur = null;
								try {
									Process erg = pb.start();
										cur = erg.getInputStream();
											int n=0;
												while ((n = cur.read()) != -1) {
														eksodos.add(String.valueOf((char)n));
													}
									}catch(IOException a) {
											a.printStackTrace();
										}
									String teliko = String.join("" , eksodos);
										/*
										 * grafw th telikh eksodo se ena arxeio 
										 * to dhmioyrgw an yparxei alliws phgainw kai grafw sthn epomenh seira
										 * 
										 */
												System.out.println(teliko);
												Writer dim  =null;
												try {
														List<String> grammes = Arrays.asList(teliko + "\n");//to apo8hkeyw kai phgainw sthn epomenh grammh
															Path myf = Paths.get("Debcoutput"+".txt");
															Files.write( myf,grammes, Charset.forName("UTF8"), StandardOpenOption.APPEND,
																		StandardOpenOption.CREATE);
																		}catch(Exception ex) {
																			ex.printStackTrace();
																			}
													}
										});
								addBehaviour(new OneShotBehaviour(this) {
												public void action() {
														System.out.println(getLocalName()+" Teleiwsa kai feygw ");
														}
															});
							}
					}