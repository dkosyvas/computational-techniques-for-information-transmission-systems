package demo;
import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jade.core.Agent;
import jade.core.Location;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;
public class WorkerClass extends Agent{
	@Override
		protected void setup() {
			Object[] args = getArguments();

			String number = String.valueOf(args[0]);
				addBehaviour(new OneShotBehaviour(this) {
						@Override
						public void action() {

							/*
							 * briskw to ektelesimo arxeio kai to trexw
							 */
							ProcessBuilder pb = new ProcessBuilder("java","-jar","/home/jim/Music/2h/ex1.jar",number);
							pb.redirectErrorStream();
							/*
							 * diavazw thn eksodo toy ektelesimoy poy einai 
							 * h aktina skedasth kai h energos diatomh σ
							 */
							ArrayList<String> eksodos = new ArrayList<String>();// edw apo8hkeyw to stream xarakthrwn apo to ektelesimo
								InputStream cur = null;
								try {
									Process erg = pb.start();						
										cur = erg.getInputStream();
											int n=0;
												while ((n = cur.read()) != -1) {
														eksodos.add(String.valueOf((char)n));
													}
									}catch(IOException a) {
											a.printStackTrace();
										}
									String teliko = String.join("" , eksodos);  // enwnw oloys toys xarakthres poy diavasa se ena string 
										/*
										 * grafw th telikh eksodo se ena arxeio 
										 * to dhmioyrgw an yparxei alliws phgainw kai grafw sthn epomenh seira
										 * 
										 */
												System.out.println(teliko);
												Writer dim  =null; // gia na grapsw to stream xarakthrwn
												try {
														List<String> grammes = Arrays.asList(teliko + "\n");//to apo8hkeyw kai phgainw sthn epomenh grammh
															Path myf = Paths.get("RCSoutput"+".txt");
															Files.write( myf,grammes, Charset.forName("UTF8"), StandardOpenOption.APPEND,
																		StandardOpenOption.CREATE);// eksasfalizw oti sto epomeno grapsimo dn 8a exw overlapping
																		}catch(Exception ex) {
																			ex.printStackTrace();
																			}
													}
										});
								addBehaviour(new OneShotBehaviour(this) {
												public void action() {
														System.out.println(getLocalName()+" Teleiwsa kai feygw ");
														}
															});
							}
					}