package demo;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import jade.core.AID;
import jade.core.Agent;
import jade.core.AgentContainer;
import jade.core.Location;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;


public class MasterClass  extends Agent{
					@Override
						protected void setup() {
							System.out.println("eimai o Master "+getLocalName()+"kai ime zwntanos");
								String MyName = getLocalName();
								addBehaviour(new OneShotBehaviour(this) {
											@Override
												public void action() {
												jade.core.Runtime runtime = jade.core.Runtime.instance();
													Profile profile = new ProfileImpl();
													profile.setParameter(Profile.CONTAINER_NAME, "PrwtoContainer");
													profile.setParameter(Profile.MAIN_HOST, "localhost");
													//dhmioyrgw container
													ContainerController container = runtime.createAgentContainer(profile);
													/*
													 * Dhmioyrgw 10 workers me onoma patata0 , patata2 ...etc patata9
													 * Toys dinw san eisodo thn aktina skedasth
													 *  [0.8, 0.8222, 0.8444, 0.8667, 0.8889, 0.9111, 0.9333, 0.9556, 0.9778, 1]
													 */
													for (int i=0 ; i<10;i++) {
														try {
															AgentController agen = container.createNewAgent("patata"+String.valueOf(i),
																	"start.WorkerClass",new Object[] {0.8 + 0.022*i});//arguments
															agen.start();
														} catch (StaleProxyException ex) {
																ex.printStackTrace();
														}
													}
											}
								});
					}
	}