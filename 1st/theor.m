N=10;
b=[0.6 0.4 0.2 0.1 0.05];
f=(0:180)*(pi/180);
moir=0:180;
k=2*pi;

for i=1:1:length(b)
    y=0;
    for n=0:1:N
        y=y+((besselj(n,k*b(i))/besselh(n,2,2*pi*b(i)))*cos(n*f))*(2*(n~=0)+1*(n==0));
    end;
    y=abs(y).^2;
    y=(4*y);
    hold on;
    plot(moir,y,'LineWidth',1); grid on;
    text(moir(end-1),y(end-1),sprintf('α=%gλ',b(i))); 
    xlim([0 180]);ylim([0 20]);xlabel('moires 0..180');ylabel('SW(σ2d/λ)')
    
end;
